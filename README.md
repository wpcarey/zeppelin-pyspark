# README

- Install Docker Desktop, and Docker Compose
- In the working directory issue the following command:
  - docker-compose up -d (in the command line)
- Visit http://localhost:8080/
- To shudown the docker container:
  - docker-compose down (in the command line)
